<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright (c) 2010 DeltaXML Ltd. All rights reserved. -->
<!-- $Id$ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
                xmlns:dxa="http://www.deltaxml.com/ns/non-namespaced-attribute"
                xmlns:math="java:java.lang.Math" 
                xmlns:saxon="http://saxon.sf.net/"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" 
                exclude-result-prefixes="xs"
                version="2.0">
  
  <!-- Tolerances are based on some multiple of the ULP, since we are comparing
    two numbers (possibly of different sizes) its wise to add their ULPs together.
    This sum is then multipled by the factor below (default value 5) and is used
    as the comparison tolerance. -->
  <xsl:param name="ulp_factor" as="xs:integer" select="5"/>
  
  <xsl:template match="@* | node()">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="*[deltaxml:textGroup[@deltaxml:deltaV2='A!=B']]
                        [deltaxml:textGroup/deltaxml:text[@deltaxml:deltaV2='A'] castable as xs:double]
                        [deltaxml:textGroup/deltaxml:text[@deltaxml:deltaV2='B'] castable as xs:double]">
    <xsl:variable name="a" as="xs:double" select="xs:double(deltaxml:textGroup/deltaxml:text[@deltaxml:deltaV2='A'])"/>
    <xsl:variable name="b" as="xs:double" select="xs:double(deltaxml:textGroup/deltaxml:text[@deltaxml:deltaV2='B'])"/>
    <xsl:variable name="a_ulp" as="xs:double" select="math:ulp($a)"/>
    <xsl:variable name="b_ulp" as="xs:double" select="math:ulp($b)"/>
    <xsl:variable name="tolerance" as="xs:double" select="$ulp_factor * ($a_ulp + $b_ulp)"/>
    <xsl:copy>
      <xsl:if test="abs($a - $b) le $tolerance">
        <xsl:attribute name="deltaxml:ignore-changes" select="'true'"/>
      </xsl:if>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="deltaxml:attributes/*[@deltaxml:deltaV2='A!=B']
                                            [deltaxml:attributeValue[@deltaxml:deltaV2='A'] castable as xs:double]
                                            [deltaxml:attributeValue[@deltaxml:deltaV2='B'] castable as xs:double]">
    <xsl:variable name="a" as="xs:double" select="xs:double(deltaxml:attributeValue[@deltaxml:deltaV2='A'])"/>
    <xsl:variable name="b" as="xs:double" select="xs:double(deltaxml:attributeValue[@deltaxml:deltaV2='B'])"/>
    <xsl:variable name="a_ulp" as="xs:double" select="math:ulp($a)"/>
    <xsl:variable name="b_ulp" as="xs:double" select="math:ulp($b)"/>
    <xsl:variable name="tolerance" as="xs:double" select="$ulp_factor * ($a_ulp + $b_ulp)"/>
    <xsl:copy>
      <xsl:if test="abs($a - $b) le $tolerance">
        <xsl:attribute name="deltaxml:ignore-changes" select="'true'"/>
      </xsl:if>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
