<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright (c) 2010 DeltaXML Ltd. All rights reserved. -->
<!-- $Id$ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
  xmlns:dxa="http://www.deltaxml.com/ns/non-namespaced-attribute"
  xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="2.0">

  <xsl:param name="temperature_tolerance" as="xs:double" select="0.1"/>
  
  <xsl:template match="@* | node()">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>
  
  <!-- 
    To check element tolerances, add their XPath(s) to this match, for example:
      temperature
      /weather/record/temperature
      /weather/record[place='Malvern']/temperature
      temperature | pressure | weight 
  -->
  <xsl:template match="temperature[deltaxml:element-in-tolerance(. , $temperature_tolerance)]">
    <xsl:copy>
      <xsl:attribute name="deltaxml:ignore-changes" select="'true'"/>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>

  <!--
    When checking for attribute tolerances the XPaths need to be mapped into the deltaV2 attribute change structure.
    For example an input attribute with XPath:
      /root/child1/grandchild/@attr1
    would be represented as an attribute change element in deltaV2 as:
      /root/child1/grandchild/deltaxml:attributes/dxa:attr1
    The checking function therefore takes the matched element as its first param
  -->
  <xsl:template match="/weather/deltaxml:attributes/dxa:time[deltaxml:attribute-in-tolerance(. , xs:double('10.0'))]">
    <xsl:copy>
      <xsl:attribute name="deltaxml:ignore-changes" select="'true'"/>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>
  
  <!-- checks elements which solely contain an (XSLT/XPath/xsd) atomic number 
    to see if the number has changed within defined attributes.
    If there are no changes there'll be no delta attributes false will be returned.
    If there are changes to the element content we should have a changed textGroup to represent it,
    if a textGroup isn't present the function hasn't been applied to an element containing atomic value.
    We need two values to calculate tolerance; if for some reason we dont have two (ie not deltaV2='A!=B')
    false will be returned. -->
  <xsl:function name="deltaxml:element-in-tolerance" as="xs:boolean">
    <xsl:param name="elem" as="element()"/>
    <xsl:param name="tolerance" as="xs:double"/>
    <xsl:sequence select="if (exists($elem/@deltaxml:deltaV2) and exists($elem/deltaxml:textGroup[@deltaxml:deltaV2='A!=B'])) then 
                            abs(number($elem/deltaxml:textGroup/deltaxml:text[@deltaxml:deltaV2='A']) - number($elem/deltaxml:textGroup/deltaxml:text[@deltaxml:deltaV2='B'])) le $tolerance
                          else
                            false()"/>
  </xsl:function>
  
  <xsl:function name="deltaxml:attribute-in-tolerance" as="xs:boolean">
    <xsl:param name="elem" as="element()"/>
    <xsl:param name="tolerance" as="xs:double"/>
    <xsl:sequence select="if (not(exists($elem/parent::deltaxml:attributes))) then 
                            error(xs:QName('error'), 'function attribute-in-tolerance not applied to deltav2 attribute')
                          else if (exists($elem/@deltaxml:deltaV2='A!=B')) then 
                            (abs(number($elem/deltaxml:attributeValue[@deltaxml:deltaV2='A']) - number($elem/deltaxml:attributeValue[@deltaxml:deltaV2='B'])) le $tolerance) 
                          else
                            false()"/>
  </xsl:function>
</xsl:stylesheet>
