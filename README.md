# Numeric Tolerances
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---

An example pipeline showing how to post-process a delta file to allow for tolerances when comparing numerical data.

This document describes how to run the sample. For concept details see: [Numeric Tolerances](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/numeric-tolerances).

If you have Ant installed, use the build script provided to run the sample. Simply type the following command to run the pipeline and produce the output file result.xml.

	ant run
	
If you don't have Ant installed, you can run the sample from a command line by issuing the following command from the sample directory (ensuring that you use the correct directory separators for your operating system).  Replace x.y.z with the major.minor.patch version number of your release e.g. deltaxml-10.0.0.jar

	java -jar ../../deltaxml-x.y.z.jar compare tolerance file1.xml file2.xml result.xml

To clean up the sample directory, run the following Ant command.

	ant clean
